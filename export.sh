#!/usr/bin/env bash
cd public

# Create all needed pages

echo "Populating 'public' directory"

tags=("title" "description" "image")

mkdir -p "strips"
for strip in "assets/strips/previews/"* ;
do
  file=${strip##*/}
  filename=${file%.*}
  cp "index.html" "strips/${filename}.html"
  for tag in "${tags[@]}"
  do
    substring=$(node ../src/helpers/strip-tags.js ${filename} ${tag})
    sed -i "s/meta_$tag/$substring/" "strips/${filename}.html"
  done
done

pages=("404" "about" "archive" "dossiers" "index" )

for page in "${pages[@]}"
do
  if [ ${page} != "index" ]; then
    cp "index.html" "${page}.html"
  fi
  for tag in "${tags[@]}"
  do
    substring=$(node ../src/helpers/page-tags.js ${page} ${tag})
    sed -i "s/meta_$tag/$substring/" "${page}.html"
  done
done
echo "Done"
