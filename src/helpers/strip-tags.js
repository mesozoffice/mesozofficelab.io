const strips = require("../assets/data/strips.json");

const page = +process.argv[2] - 1;
const strip = strips[page];
const name = process.argv[3];
let value = strip[name];
if (name == "title") {
  value = value + " | Mesozoffice Comics";
}
if (name == "description") {
  value = "Read the full strip here (EN or PT)";
}
if (name == "image") {
  value = `https://mesozoffice.com/assets/strips/previews/${strip.number}.jpg`;
}
console.log(value.replaceAll("/", "\\/"));
