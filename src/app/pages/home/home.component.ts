import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { MO_Strip } from '../../interfaces';
import { StripService } from '../../services/strip.service';
import meta from '../../../assets/data/meta.json';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less'],
})
export class HomePage implements OnInit {
  strip: MO_Strip;
  lastNum: number;

  constructor(
    private stripService: StripService,
    private titleService: Title,
    private metaService: Meta,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.updateTags();
    this.setStrip();
  }

  updateTags() {
    this.titleService.setTitle(meta.index.title);
    this.metaService.updateTag({ name: 'description', content: meta.index.description });
    this.metaService.updateTag({ name: 'og:title', content: meta.index.title });
    this.metaService.updateTag({ name: 'og:description', content: meta.index.description });
    this.metaService.updateTag({ name: 'og:image', content: meta.index.image });
    this.metaService.updateTag({ name: 'og:image:alt', content: meta.index.title });
    this.metaService.updateTag({ name: 'twitter:title', content: meta.index.title });
    this.metaService.updateTag({ name: 'twitter:description', content: meta.index.description });
    this.metaService.updateTag({ name: 'twitter:image', content: meta.index.image });
    this.metaService.updateTag({ name: 'twitter:image:alt', content: meta.index.title });
    this.metaService.updateTag({ name: 'twitter:card', content: 'summary' });
  }

  setStrip() {
    let snapshot = this.route.snapshot;
    let number = snapshot.paramMap.get('number');
    const lastStrip = this.stripService.getLast();
    this.strip = number ? this.stripService.getByNumber(+number) : lastStrip;
    this.lastNum = lastStrip.number;
  }

  next() {
    this.strip = this.stripService.getNext(this.strip);
    this.go(this.strip.number);
  }

  prev() {
    this.strip = this.stripService.getPrev(this.strip);
    this.go(this.strip.number);
  }

  rand() {
    this.strip = this.stripService.getRandom(this.strip);
    this.go(this.strip.number);
  }

  go(stripNumber: number) {
    this.router.navigate(
      [`strips/${stripNumber}`],
      {
        queryParamsHandling: "merge"
      }
    );
  }
}
