import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import meta from '../../../assets/data/meta.json';

@Component({
  selector: 'app-dossiers',
  templateUrl: './dossiers.component.html',
  styleUrls: ['./dossiers.component.less']
})
export class DossiersPage implements OnInit {
  constructor(
    private titleService: Title,
    private metaService: Meta,
    ) {

    }

  ngOnInit(): void {
    this.updateTags();
  }

  updateTags() {
    this.titleService.setTitle(meta.dossiers.title);
    this.metaService.updateTag({ name: 'description', content: meta.dossiers.description });
    this.metaService.updateTag({ name: 'og:title', content: meta.dossiers.title });
    this.metaService.updateTag({ name: 'og:description', content: meta.dossiers.description });
    this.metaService.updateTag({ name: 'og:image', content: meta.dossiers.image });
    this.metaService.updateTag({ name: 'og:image:alt', content: meta.dossiers.title });
    this.metaService.updateTag({ name: 'twitter:title', content: meta.dossiers.title });
    this.metaService.updateTag({ name: 'twitter:description', content: meta.dossiers.description });
    this.metaService.updateTag({ name: 'twitter:image', content: meta.dossiers.image });
    this.metaService.updateTag({ name: 'twitter:image:alt', content: meta.dossiers.title });
    this.metaService.updateTag({ name: 'twitter:card', content: 'summary' });
  }
}
