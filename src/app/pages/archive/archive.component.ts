import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

import { MO_Strip } from '../../interfaces';
import { StripService }  from '../../services/strip.service';
import meta from '../../../assets/data/meta.json';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.less']
})
export class ArchivePage implements OnInit {
  strips: MO_Strip[];

  constructor(
    private stripService: StripService,
    private titleService: Title,
    private metaService: Meta,
  ) { }

  ngOnInit(): void {
    this.strips = this.stripService.getStrips();
    this.updateTags();
  }

  updateTags() {
    this.titleService.setTitle(meta.archive.title);
    this.metaService.updateTag({ name: 'description', content: meta.archive.description });
    this.metaService.updateTag({ name: 'og:title', content: meta.archive.title });
    this.metaService.updateTag({ name: 'og:description', content: meta.archive.description });
    this.metaService.updateTag({ name: 'og:image', content: meta.archive.image });
    this.metaService.updateTag({ name: 'og:image:alt', content: meta.archive.title });
    this.metaService.updateTag({ name: 'twitter:title', content: meta.archive.title });
    this.metaService.updateTag({ name: 'twitter:description', content: meta.archive.description });
    this.metaService.updateTag({ name: 'twitter:image', content: meta.archive.image });
    this.metaService.updateTag({ name: 'twitter:image:alt', content: meta.archive.title });
    this.metaService.updateTag({ name: 'twitter:card', content: 'summary' });
  }
}
