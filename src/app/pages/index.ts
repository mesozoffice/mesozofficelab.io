
export { DossiersPage } from './dossiers/dossiers.component';
export { HomePage } from './home/home.component';
export { AboutPage } from './about/about.component';
export { ArchivePage } from './archive/archive.component';
export { ErrorPage } from './error/error.component';
