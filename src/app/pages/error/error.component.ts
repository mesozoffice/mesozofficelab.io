import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import meta from '../../../assets/data/meta.json';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.less'],
})
export class ErrorPage implements OnInit {
  constructor(
    private titleService: Title,
    private metaService: Meta,) {}

  ngOnInit(): void {
    this.updateTags();
  }

  updateTags() {
    this.titleService.setTitle(meta['404'].title);
    this.metaService.updateTag({ name: 'description', content: meta['404'].description });
    this.metaService.updateTag({ name: 'og:title', content: meta['404'].title });
    this.metaService.updateTag({ name: 'og:description', content: meta['404'].description });
    this.metaService.updateTag({ name: 'og:image', content: meta['404'].image });
    this.metaService.updateTag({ name: 'og:image:alt', content: meta['404'].title });
    this.metaService.updateTag({ name: 'twitter:title', content: meta['404'].title });
    this.metaService.updateTag({ name: 'twitter:description', content: meta['404'].description });
    this.metaService.updateTag({ name: 'twitter:image', content: meta['404'].image });
    this.metaService.updateTag({ name: 'twitter:image:alt', content: meta['404'].title });
    this.metaService.updateTag({ name: 'twitter:card', content: 'summary' });
  }
}
