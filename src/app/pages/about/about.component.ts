import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import meta from '../../../assets/data/meta.json';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.less'],
})
export class AboutPage implements OnInit {
  constructor(
    private titleService: Title,
    private metaService: Meta,
  ) {
  }

  ngOnInit(): void {
    this.updateTags();
  }

  updateTags() {
    this.titleService.setTitle(meta.about.title);
    this.metaService.updateTag({ name: 'description', content: meta.about.description });
    this.metaService.updateTag({ name: 'og:title', content: meta.about.title });
    this.metaService.updateTag({ name: 'og:description', content: meta.about.description });
    this.metaService.updateTag({ name: 'og:image', content: meta.about.image });
    this.metaService.updateTag({ name: 'og:image:alt', content: meta.about.title });
    this.metaService.updateTag({ name: 'twitter:title', content: meta.about.title });
    this.metaService.updateTag({ name: 'twitter:description', content: meta.about.description });
    this.metaService.updateTag({ name: 'twitter:image', content: meta.about.image });
    this.metaService.updateTag({ name: 'twitter:image:alt', content: meta.about.title });
    this.metaService.updateTag({ name: 'twitter:card', content: 'summary' });
  }
}
