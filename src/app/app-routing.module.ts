import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutPage, ArchivePage, DossiersPage, ErrorPage, HomePage  } from './pages';

const routes: Routes = [
  { path: 'home', redirectTo: '', pathMatch: 'full' },
  { path: 'strips', redirectTo: '', pathMatch: 'full' },
  { path: '', pathMatch: 'full' , component: HomePage },
  { path: 'about', component: AboutPage },
  { path: 'strips',
    children: [
      { path: ':number', component: HomePage }
    ]
  },
  { path: 'archive', component: ArchivePage },
  { path: 'archive',
    children: [
      { path: ':number', component: ArchivePage }
    ]
  },
  { path: 'dossiers', component: DossiersPage },
  { path: '**', component: ErrorPage },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
