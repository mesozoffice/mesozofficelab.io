import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import strips from '../../assets/data/strips.json';

import { MO_Strip } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class StripService {
  strips: MO_Strip[];

  constructor(
    private translate: TranslateService,
  ) {
    this.mapStrips();
  }

  mapStrips():void {
    this.strips = strips.map((strip,index) => {
      const stripObj = {
        ...strip,
        title: `strips.${index}.title`,
        files: strip.filename.split('|'),
        preview: `assets/strips/previews/${strip.number}.jpg`
      }
      return stripObj;
    });
  }

  getStrips(): MO_Strip[] {
    return this.strips;
  }

  getPersonalStrips(id: string): MO_Strip[] {
    const strips = this.getStrips();
    let personalStrips = [];
    for (let strip of strips) {
      for (let party of strip.participants) {
        if (party == id) {
          personalStrips.push(strip);
          break;
        }
      }
    }
    return personalStrips;
  }

  getByNumber(number: number): MO_Strip | undefined {
    if (isNaN(number)) return;
    const strips = this.getStrips();
    const found = strips.find(strip => strip.number == number);
    return found;
  }

  getLast(): MO_Strip {
    const strips = this.getStrips();
    return strips[strips.length - 1];
  }

  getLink(strip: MO_Strip) {
    const lang = this.translate.currentLang || this.translate.defaultLang;
    return `${window.location.origin}/strips/${strip.number}?lang=${lang}`;
  }

  getNext(strip: MO_Strip) {
    const strips = this.getStrips();
    const i = strips.indexOf(strip);
    if (i < strips.length) {
      return strips[i+1];
    }
    return strip;
  }

  getPrev(strip: MO_Strip) {
    const strips = this.getStrips();
    const i = strips.indexOf(strip);
    if (i > 0) {
      return strips[i-1];
    }
    return strip;
  }

  getRandom(currStrip?: MO_Strip) {
    // TODO: implement the CORRECT way -_-
    const strips = this.getStrips();
    let random: MO_Strip;
    do {
      let i = Math.floor(Math.random() * strips.length);
      random = strips[i];
    }
    while (random.number == currStrip.number)

    return random;
  }
}
