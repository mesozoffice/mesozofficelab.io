import { Injectable } from '@angular/core';
import employees from '../../assets/data/employees.json';
import { MO_Genus } from '../enums';
import { MO_Employee } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor() {}

  getEmployees(searchString?: string, sortField?: string, sortDirection?: string): MO_Employee[] {
    function search(str: string): boolean {
      return str.toLowerCase().includes(searchString.toLowerCase());
    }
    function searchArray(arr: string[]): boolean {
      let found = false;
      for (let el of arr) {
        if (el.toLowerCase().includes(searchString.toLowerCase())) {
          found = true;
          break;
        }
      }
      return found;
    }
    function compare(a: number | string, b: number | string, isAsc: boolean) {
      return (a <= b ? -1 : 1) * (isAsc ? 1 : -1);
    }

    let filteredData: MO_Employee[];
    //console.log(searchString)
    if (searchString && searchString != "") {
      filteredData = employees.filter((employee) => {
        if (
          search(employee.firstName) ||
          search(employee.lastName) ||
          search(employee.genus) ||
          search(employee.email) ||
          search(employee.position) ||
          searchArray(employee.interests) ||
          search(employee.about)
        ) {
          //console.log(employee)
          return true;
        } else {
          return false;
        }
      });
    } else {
      filteredData = employees;
    }
    let sortedData: MO_Employee[];
    //console.log(sortField, sortDirection)
    if (sortField && sortDirection) {
      sortedData = filteredData.sort((a,b) => {
        const isAsc = sortDirection === 'asc';
        switch (sortField) {
          case 'birthdate': return compare(a.birthdate.toDateString(), b.birthdate.toDateString(), isAsc);
          default: return compare(a[sortField], b[sortField], isAsc);
        }
      });
    } else {
      sortedData = filteredData;
    }
    return sortedData;
  }

  getEmployee(id: string): MO_Employee {
    let chosen;
    for (let employee of employees) {
      if (employee.id == id) {
        chosen = employee;
        break;
      }
    }
    return chosen;
  }

  getEmptyEmployee(): MO_Employee {
    let employee = {
        "id":"",
        "avatar": "",
        "firstName": "",
        "lastName": "",
        "genus": MO_Genus.none,
        "birthdate": new Date(),
        "email": "",
        "about": "",
        "interests": [],
        "department": "",
        "position": ""
    };
    return employee;
  }

  getAvatar(id: string): string {
    return "assets/avatars/"+id+".jpg";
  }

  getEmptyAvatar(): string {
    return "assets/avatars/unknown.jpg";
  }
}
