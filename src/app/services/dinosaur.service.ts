import { Injectable } from '@angular/core';

import { MO_Genus } from '../enums';

@Injectable({
  providedIn: 'root'
})
export class DinosaurService {

  constructor() { }

  getInfoLink(genus: MO_Genus) {
    switch (genus) {
      case MO_Genus.ankylosaurus:
          return "https://en.wikipedia.org/wiki/Ankylosaurus";
      case MO_Genus.carnotaurus:
          return "https://en.wikipedia.org/wiki/Carnotaurus";
      case MO_Genus.diplodocus:
        return "https://en.wikipedia.org/wiki/Diplodocus";
      case MO_Genus.edmontosaurus:
        return "https://en.wikipedia.org/wiki/Edmontosaurus";
      case MO_Genus.elaphrosaurus:
          return "https://en.wikipedia.org/wiki/Elaphrosaurus";
      case MO_Genus.pachycephalosaurus:
          return "https://en.wikipedia.org/wiki/Pachycephalosaurus";
      case MO_Genus.pteranodon:
          return "https://en.wikipedia.org/wiki/Pteranodon";
      case MO_Genus.stegosaurus:
          return "https://en.wikipedia.org/wiki/Stegosaurus";
      case MO_Genus.triceratops:
          return "https://en.wikipedia.org/wiki/Triceratops";
      case MO_Genus.tyrannosaurus:
          return "https://en.wikipedia.org/wiki/Tyrannosaurus";
      case MO_Genus.velociraptor:
          return "https://en.wikipedia.org/wiki/Velociraptor";
    }
  }
}
