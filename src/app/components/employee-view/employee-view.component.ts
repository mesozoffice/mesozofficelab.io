import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { MO_Employee, MO_Strip } from '../../interfaces';
import { MO_Genus } from '../../enums';
import { DinosaurService }  from '../../services/dinosaur.service';
import { EmployeeService }  from '../../services/employee.service';
import { StripService }  from '../../services/strip.service';

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee-view.component.less']
})
export class EmployeeViewComponent implements OnInit {

  @Input() employeeId: string;
  @Output() search = new EventEmitter();
  employee : MO_Employee;
  genusEnum = MO_Genus;
  strips: MO_Strip[];
  activeTab = 0;

  constructor(
    private employeeService: EmployeeService,
    private dinosaurService: DinosaurService,
    private stripService: StripService,
  ) { }

  ngOnInit(): void {
    this.getEmployee();
  }

  getEmployee(): void {
    this.employee = this.employeeService.getEmployee(this.employeeId)
    this.strips = this.stripService.getPersonalStrips(this.employee.id);
  }

  getAvatar(): String {
    return this.employeeService.getAvatar(this.employee.id);
  }

  getGenusLink(): String {
    return this.dinosaurService.getInfoLink(MO_Genus[this.employee.genus]);
  }

  selectActiveTab(num: number) {
    this.activeTab = num;
  }

  searchInterests(interest: string): void {
    this.search.emit({search: interest});
  }

}
