import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.less'],
})
export class ToolbarComponent {
  title = 'mesozoffice';
  locale = 'en';
  component = 'home';
  menuOpen = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private translate: TranslateService,
  ) {
  }

  ngOnInit() {
    this.translate.addLangs(['en', 'pt', 'ru']);
    this.translate.setDefaultLang('en');
    this.route.queryParams.subscribe(params => {
      const locale = params['lang'];
      if (locale && locale.match(/en|pt|ru/)) {
        this.translate.use(locale);
        this.locale = locale;
      }
    })
    const store = this.translate.store;
    this.locale = store.currentLang || store.defaultLang;
  }

  navigate(tab: string) {
    this.router.navigate([tab],{ queryParamsHandling: "merge" });
  }

  toggleLanguage(locale: string) {
    this.router.navigate([],{
      relativeTo: this.route,
      queryParams: { lang: locale },
      queryParamsHandling: 'merge'
    })
  }

  localeClass(locale: string): string {
    return this.locale === locale? 'selected': '';
  }

  toggleMenu() {
    this.menuOpen = !this.menuOpen;
  }
}
