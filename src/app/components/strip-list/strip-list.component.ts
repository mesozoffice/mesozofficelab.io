import { Component, Input, Output, EventEmitter } from '@angular/core';

import { MO_Strip } from '../../interfaces';
import { EmployeeService } from '../../services/employee.service';

@Component({
  selector: 'app-strip-list',
  templateUrl: './strip-list.component.html',
  styleUrls: ['./strip-list.component.less']
})
export class StripListComponent {
  @Input() strips: MO_Strip[];
  @Input() showParticipants = true;
  @Output() select = new EventEmitter();

  constructor(
    private employeeService: EmployeeService,
  ) { }

  getAvatar(id: string): String {
    return this.employeeService.getAvatar(id);
  }

  selectStrip(num: number) {
    this.select.emit({num});
  }

}
