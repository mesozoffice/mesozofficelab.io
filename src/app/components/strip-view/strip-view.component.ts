import { Location } from '@angular/common';
import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MO_Strip } from '../../interfaces';

@Component({
  selector: 'app-strip-view',
  templateUrl: './strip-view.component.html',
  styleUrls: ['./strip-view.component.less']
})
export class StripViewComponent {
  @Input() strips: MO_Strip[];
  @Input() showParticipants = true;
  _strips: MO_Strip[];
  selected = -1;

  constructor(
    private location: Location,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this._strips = this.strips.slice().reverse();
    let snapshot = this.route.snapshot;
    let number = +snapshot.paramMap.get('number');
    this.selected = number ? this._strips.findIndex(strip => strip.number == number) : -1;
  }

  select(num: number) {
    this.selected = num;
    this.location.go(`/archive/${this._strips[this.selected].number}`);
  }

  closeOverlay() {
    this.selected = -1;
    this.location.go(`/archive`);
  }

  onStripClick($event) {
    this.select($event.num);
  }

}
