import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MO_Employee } from '../../interfaces';
import { MO_EmployeeAction } from '../../enums';
import { EmployeeService } from '../../services/employee.service';
import meta from '../../../assets/data/meta.json';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.less']
})
export class EmployeeListComponent implements OnInit {
  employees: MO_Employee[];
  selected: string;
  action = MO_EmployeeAction.none;
  searchString = "";
  sortBy: string;
  sortDirection: string;
  columnsToDisplay = [
    'firstName',
    'lastName',
    'position',
    'department',
    'email',
  ];

  constructor(
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private router: Router,
    ) {

    }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(params => { this.parseQueryParams(); })
    this.sortDirection = "asc";
    this.sortBy = "firstName";
    this.getEmployees();
  }

  parseQueryParams(): void {
    let snapshot = this.route.snapshot;
    this.selected = snapshot.queryParamMap.get('id');
    let searchString = snapshot.queryParamMap.get('search');
    if (searchString) {
      this.searchString = searchString;
      this.getEmployees()
    }
  }

  clearSearchField() {
    this.searchString = "";
    this.getEmployees();
    this.router.navigate([],{
      relativeTo: this.route,
      queryParams: { search: null },
      queryParamsHandling: 'merge'
    });
  }

  view(employee: MO_Employee) {
    // window.scrollTo(0,0);
    this.action = MO_EmployeeAction.view;
    this.selected = employee.id;
    this.router.navigate([],{
      relativeTo: this.route,
      queryParams: { id: employee.id },
      queryParamsHandling: 'merge'
    });
  }

  onSearchUpdate($event): void {
    this.router.navigate([],{
      relativeTo: this.route,
      queryParams: { search: $event.search, id: null },
      queryParamsHandling: 'merge',
    });
  }

  getEmployees(): void {
    this.employees = this.employeeService.getEmployees(this.searchString, this.sortBy, this.sortDirection);
  }

  linkTo(id: string) {
    return `/dossiers/${id}`
  }

  sort(prop: string) {
    if (this.sortBy == prop) {
      this.sortDirection = this.sortDirection == 'asc'? 'desc': 'asc';
    } else {
      this.sortBy = prop;
      this.sortDirection = 'asc';
    }
    this.getEmployees()
  }

  toggleEmployeeView() {
    this.router.navigate([],{
      relativeTo: this.route,
      queryParams: { id: null },
      queryParamsHandling: 'merge'
    });
  }

}
