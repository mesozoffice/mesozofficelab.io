import { Component, Input, HostListener, Output, EventEmitter } from '@angular/core';

import { MO_Strip } from '../../interfaces';

@Component({
  selector: 'app-strip-overlay',
  templateUrl: './strip-overlay.component.html',
  styleUrls: ['./strip-overlay.component.less']
})
export class StripOverlayComponent {
  @Input() strips: MO_Strip[];
  @Input() selected = -1;
  @Output() close = new EventEmitter();
  @Output() select = new EventEmitter();

  @HostListener('window:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (this.selected >= 0) {
      if (event.key == "ArrowRight") {
        this.next(event);
      }
      if (event.key == "ArrowLeft") {
        this.prev(event);
      }
    }
  }

  constructor(
  ) { }

  getSelected(): MO_Strip {
    return this.strips[this.selected];
  }

  next(event: Event) {
    event.stopPropagation();
    let num = this.selected + 1;
    if (num >= this.strips.length) {
      return;
    }
    this.onSelect(num);
  }

  prev(event: Event) {
    event.stopPropagation();
    let num = this.selected - 1;
    if (num < 0) {
      return;
    }
    this.onSelect(num);
  }

  onSelect(num: number) {
    this.select.emit({num});
  }

  onClose() {
    this.close.emit();
  }

}
