import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarComponent } from '../../shared/components';
import { MO_Strip } from '../../interfaces';
import { StripService } from '../../services/strip.service'

@Component({
  selector: 'app-strip-card',
  templateUrl: './strip-card.component.html',
  styleUrls: ['./strip-card.component.less'],
})
export class StripCardComponent implements OnInit {
  @Input() strip: MO_Strip;
  @Input() width = 700;
  @ViewChild('snackbar') snackbar: SnackbarComponent;
  selectedFile = 0;
  lang: string;
  _fileCount = 1;
  _touchstart = { clientX: 0, clientY: 0};
  _touchend = { clientX: 0, clientY: 0};

  constructor(
    private stripService: StripService,
    private translate: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.setLang();
    const self = this;
    this.translate.store.onLangChange.subscribe(() => {
      self.setLang();
    });
  }

  setLang() {
    const store = this.translate.store;
    this.lang = store.currentLang || store.defaultLang;
  }

  ngOnChanges(): void {
    this._fileCount = this.strip.filename.split('|').length;
    this.selectedFile = 0;
  }

  getLink(): string {
    let link = this.stripService.getLink(this.strip);
    return link;
  }

  getTwitterLink() {
    return `https://twitter.com/share?text=Check%20out%20this%20%40Mesozoffice%20strip&url=${this.getLink()}`;
  }

  getFacebookLink() {
    return `https://www.facebook.com/sharer/sharer.php?u=${this.getLink()}`;
  }

  getRedditLink() {
    return `http://www.reddit.com/submit?url=${this.getLink()}`;
  }

  showSnackBar() {
    this.snackbar.show(this.translate.instant('card.confirmation'));
  }

  showDots() {
    return this._fileCount > 1;
  }

  nextFile() {
    if (this.selectedFile < this._fileCount - 1) {
      this.selectedFile++;
    }
  }

  prevFile() {
    if (this.selectedFile > 0) {
      this.selectedFile--;
    }
  }

  select(num: number) {
    this.selectedFile = num;
  }

  onTouchStart(e) {
    this._touchstart = e.changedTouches[0];
  }

  onTouchEnd(e) {
    this._touchend = e.changedTouches[0];
    const deltaX = this._touchend.clientX - this._touchstart.clientX;
    const deltaY = this._touchend.clientY - this._touchstart.clientY;
    // this is horizontal swipe
    if (Math.abs(deltaX) > Math.abs(deltaY)) {
      if (deltaX < 0) {
        this.nextFile();
      } else {
        this.prevFile();
      }
    }
  }
}
