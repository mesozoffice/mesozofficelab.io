import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
})
export class AppComponent {
  title = 'mesozoffice';
  locale = 'en';
  component = 'home';

  constructor(
    private route: ActivatedRoute,
    private translate: TranslateService,
  ) {
  }

  ngOnInit() {
    this.translate.addLangs(['en', 'pt', 'ru']);
    this.translate.setDefaultLang('en');
    this.route.queryParams.subscribe(params => {
      const locale = params['lang'];
      if (locale && locale.match(/en|pt|ru/)) {
        this.translate.use(locale);
        this.locale = locale;
      }
    })
    const store = this.translate.store;
    this.locale = store.currentLang || store.defaultLang;
  }
}
