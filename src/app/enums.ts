export enum MO_EmployeeAction {
  none    = 0,
  edit    = 1,
  new     = 2,
  delete  = 3,
  view    = 4,
}

export enum MO_Genus {
  none                = "Unspecified",
  ankylosaurus        = "Ankylosaurus",
  carnotaurus         = "Carnotaurus",
  diplodocus          = "Diplodocus",
  edmontosaurus       = "Edmontosaurus",
  elaphrosaurus       = "Elaphrosaurus",
  pachycephalosaurus  = "Pachycephalosaurus",
  pteranodon          = "Pteranodon",
  stegosaurus         = "Stegosaurus",
  triceratops         = "Triceratops",
  tyrannosaurus       = "Tyrannosaurus",
  velociraptor        = "Velociraptor",
}
