import { Component, ElementRef, HostBinding, Input } from '@angular/core';

@Component({
  selector: '[m-icon-button]',
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.less'],
})
export class IconButtonComponent {
  @Input() icon: string;
  @Input() theme: 'light' | 'dark' = "light";
  @Input() size: 'sm' | 'md' | 'lg' = "md";
  @HostBinding('class') get className() {
    return `${this.theme} ${this.size}`;
  }
  path: string;

  constructor(public button: ElementRef) {
  }

  ngOnInit() {
    this.path = `assets/svg/${this.icon}.svg`;
  }



}
