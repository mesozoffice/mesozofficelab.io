import { Component } from '@angular/core';

@Component({
  selector: 'app-snack-bar',
  templateUrl: './snack-bar.component.html',
  styleUrls: ['./snack-bar.component.less'],
})
export class SnackbarComponent {
  message = '';
  display: 'block' | 'none' = 'none';

  constructor(
  ) {
  }

  show(message: string) {
    this.message = message;
    this.display = 'block';
    setTimeout(() => {
      this.message = '';
      this.display = 'none';
    }, 1500)
  }
}
