import { Component, EventEmitter, HostListener, Output, ViewChild } from '@angular/core';
import { IconButtonComponent } from '..';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.less'],
})
export class PopupComponent {
  @Output() toggle = new EventEmitter();
  @ViewChild("closebutton") closeButton: IconButtonComponent;


  @HostListener('window:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
      if (event.key == "Escape") {
        this.toggle.emit();
      }
  }

  constructor(
  ) {
  }

  ngAfterViewInit() {
    this.closeButton.button.nativeElement.focus();
  }

  onClick() {
    this.toggle.emit();
  }
}
