import { Directive, ElementRef, HostBinding, HostListener, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Directive({
  selector: '[m-nav-tab]'
})
export class NavTabDirective {
  @Input() route = '';

  constructor (
    private el: ElementRef,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    const nativeElement = this.el.nativeElement;
    nativeElement.classList.add("nav-tab");
    this.activatedRoute.queryParams.subscribe(params => {
      let queryStrong = params['lang']? `?lang=${params['lang']}`:'';
      nativeElement.setAttribute('href', `${this.route}${queryStrong}`);
    })
  }


  @HostListener('click') onClick() {
    this.navigate(this.route);
  }

  @HostBinding('class.selected') get selected() { return this.isActiveRoute(); }

  isActiveRoute(): boolean {
    const isActiveExactly = this.router.isActive(
      this.route,
      {
        paths: 'exact',
        queryParams: 'ignored',
        fragment:'ignored',
        matrixParams:'ignored'
      });
    const isStrips = this.router.url.includes("strips");
    return isActiveExactly || (this.route == '/' && isStrips);
  }

  navigate(tab: string) {
    this.router.navigate(
      [tab],
      {
        queryParams: {search: null, id: null},
        queryParamsHandling: "merge"
      }
      );
  }

}
