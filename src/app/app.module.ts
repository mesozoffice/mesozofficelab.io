import { BrowserModule } from '@angular/platform-browser';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { HttpClient, HttpClientModule }    from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AboutPage, ArchivePage, DossiersPage, ErrorPage, HomePage  } from './pages';
import { StripViewComponent } from './components/strip-view/strip-view.component';
import { StripCardComponent } from './components/strip-card/strip-card.component';
import { StripListComponent } from './components/strip-list/strip-list.component';
import { StripOverlayComponent } from './components/strip-overlay/strip-overlay.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { EmployeeViewComponent } from './components/employee-view/employee-view.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { IconButtonComponent, PopupComponent, MenuItemsComponent, SnackbarComponent } from './shared/components';
import { NavTabDirective } from './shared/directives/index';

@NgModule({
  declarations: [
    AppComponent,
    // Pages
    HomePage,
    ArchivePage,
    AboutPage,
    DossiersPage,
    ErrorPage,
    // Components
    StripViewComponent,
    StripCardComponent,
    StripListComponent,
    StripOverlayComponent,
    EmployeeListComponent,
    EmployeeViewComponent,
    ToolbarComponent,
    MenuItemsComponent,
    PopupComponent,
    IconButtonComponent,
    SnackbarComponent,
    NavTabDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ClipboardModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}
