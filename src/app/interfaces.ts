import { MO_Genus } from './enums';

export interface MO_Employee {
  id: string;
  avatar: string;
  firstName: string;
  lastName: string;
  genus: MO_Genus;
  email: string;
  birthdate: Date;
  about: string;
  interests: string[];
  department: string;
  position: string;
}


export interface MO_Strip {
  number: number;
  filename: string;
  files: string[];
  preview: string;
  title: string;
  date: string;
  participants: string[];
}
